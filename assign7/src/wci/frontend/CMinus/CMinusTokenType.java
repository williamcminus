package wci.frontend.CMinus;

import wci.frontend.TokenType;

import java.util.HashSet;
import java.util.Hashtable;

/**
 * <h1>CMinusTokenType</h1>
 * <p/>
 * <p>CMinus token types.</p>
 */
public enum CMinusTokenType implements TokenType {
    // Reserved words.
    ELSE, IF, RETURN, VOID, WHILE,

    // Special symbols.
    PLUS("+"), MINUS("-"), STAR("*"), SLASH("/"), LESS_THAN("<"),
    LESS_EQUALS("<="), GREATER_THAN(">"), GREATER_EQUALS(">="),
    EQUALS("=="), NOT_EQUALS("!="), GETS("="), SEMICOLON(";"),
    COMMA(","), LEFT_BRACE("{"), RIGHT_BRACE("}"), LEFT_PAREN("("),
    RIGHT_PAREN(")"), LEFT_BRAKET("["), RIGHT_BRAKET("]"),
    NOT("!"),AND("&&"),OR("||"),


    IDENTIFIER, INTEGER, REAL, ERROR, END_OF_FILE;

    private static final int FIRST_RESERVED_INDEX = ELSE.ordinal();
    private static final int LAST_RESERVED_INDEX = WHILE.ordinal();

    private static final int FIRST_SPECIAL_INDEX = PLUS.ordinal();
    private static final int LAST_SPECIAL_INDEX = OR.ordinal();

    private String text;  // token text

    /**
     * Constructor.
     */
    CMinusTokenType() {
        this.text = this.toString().toLowerCase();
    }

    /**
     * Constructor.
     *
     * @param text the token text.
     */
    CMinusTokenType(String text) {
        this.text = text;
    }

    /**
     * Getter.
     *
     * @return the token text.
     */
    public String getText() {
        return text;
    }

    // Set of lower-cased CMinus reserved word text strings.
    public static HashSet<String> RESERVED_WORDS = new HashSet<String>();

    static {
        CMinusTokenType values[] = CMinusTokenType.values();
        for (int i = FIRST_RESERVED_INDEX; i <= LAST_RESERVED_INDEX; ++i) {
            RESERVED_WORDS.add(values[i].getText().toLowerCase());
        }
    }

    // Hash table of CMinus special symbols.  Each special symbol's text
    // is the key to its CMinus token type.
    public static Hashtable<String, CMinusTokenType> SPECIAL_SYMBOLS =
            new Hashtable<String, CMinusTokenType>();

    static {
        CMinusTokenType values[] = CMinusTokenType.values();
        for (int i = FIRST_SPECIAL_INDEX; i <= LAST_SPECIAL_INDEX; ++i) {
            SPECIAL_SYMBOLS.put(values[i].getText(), values[i]);
        }
    }
}
