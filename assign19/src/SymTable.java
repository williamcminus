import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 4/25/11
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class SymTable extends Hashtable<String,SymTableEntry> {
    public int memUsage;

    public SymTable() {
        super();

        memUsage = 0;
    }

    public SymTableEntry lookupLocal(String identifier) {
        return get(identifier);
    }

    public void enterLocalParam(String identifier, SymTableEntry entry) {
        put(identifier,entry);
    }

    public void enterLocal(String identifier, String type, String spec) {
        put(identifier,new SymTableEntry(type, spec, SymTableStack.currentStackPos, 1));
        SymTableStack.currentStackPos++;
        memUsage++;
    }


    public void enterLocalArray(String identifier, String type, String spec, int length) {
        put(identifier,new SymTableEntry(type, spec, SymTableStack.currentStackPos, length));
        SymTableStack.currentStackPos += length;
        memUsage += length;
    }
}
