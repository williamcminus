/* Generated By:JJTree: Do not edit this line. ASTtermPrime.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ASTtermPrime extends SimpleNode {
  public ASTtermPrime(int id) {
    super(id);
  }

  public ASTtermPrime(CminusParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(CminusParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=8a7cd7ba11d62a3c547f64d3bb098ee6 (do not edit this line) */
