import java.io.*;
import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 4/25/11
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class CMMVisitor implements CminusParserVisitor {
    FileWriter fw;
    PrintWriter out;

    SymTableStack symTableStack;
    Stack<Object> exprStack;
    Stack<Object> argsStack;

    Flag flag;

    public Object visit(SimpleNode node, Object data) {
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTinteger node, Object data) {
        node.childrenAccept(this, data);
        return node.value;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTprogram node, Object data) {
        symTableStack = new SymTableStack();
        exprStack = new Stack<Object>();
        argsStack = new Stack<Object>();
        try {
            fw = new FileWriter("out.cmm_asm");
            out = new PrintWriter(fw);
        } catch (Exception e) {
            e.printStackTrace();
        }

        flag = Flag.EXPRESSION;

        node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.


        out.close();

        return data;
    }

    public Object visit(ASTdeclarationList node, Object data) {
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTdeclaration node, Object data) {
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTtypeSpecifier node, Object data) {
        node.childrenAccept(this, null);
        return node.value;
    }

    public Object visit(ASTvarDecleration node, Object data) {

        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTscalarDeclaration node, Object data) {
        Node[] children = node.children;
        String identifier = node.value.toString();
        Object type = children[0].jjtAccept(this, data);

        symTableStack.getLocal().enterLocal(identifier, type.toString(), "var");

        /* generate the asm */
        out.printf("OPCODE(mov) SRC1(R_zero) DEST(TOS) // %s %s;\n", type, identifier);

        return data;
    }

    public Object visit(ASTarrayDeclaration node, Object data) {
        Node[] children = node.children;
        String identifier = node.value.toString();
        Object type = children[0].jjtAccept(this, data);
        int elementCount = Integer.parseInt(children[1].jjtAccept(this, data).toString());

        symTableStack.getLocal().enterLocalArray(identifier, type.toString(), "array", elementCount);

        /* generate the asm */
        for (int i = 0; i < elementCount; i++) {
            out.printf("OPCODE(mov) SRC1(R_zero) DEST(TOS) // %s %s[%s];\n", type, identifier, i);
        }

        return data;
    }

    public Object visit(ASTfunDecleration node, Object data) {
        Node[] children = node.children;
        String functionName = node.value.toString();

        if (functionName.equals("main")) {
            generateMainHeader();
        }

        String type = children[0].jjtAccept(this,data).toString();

        SymTable tableWithParams = (SymTable) children[1].jjtAccept(this,data);

        out.printf("LABEL(%s) OPCODE(nop)\n", functionName);

        /* generate the function body code */
        children[2].jjtAccept(this,tableWithParams);

        out.printf("OPCODE(ret)\n");

        return data;
    }

    private void generateMainHeader() {
        out.printf("//\n" +
                "LABEL(start) OPCODE(nop)\n" +
                "OPCODE(call) DEST(main)\n" +
                "OPCODE(exit)\n");
    }

    public Object visit(ASTparams node, Object data) {
        if(node.children == null) {
            return null;
        }

        Node[] children = node.children;

        /* create new symTable so we can throw in the params */
        SymTable symTable = new SymTable();

        for(int i = 0; i < children.length; i++) {
            ASTparam param = (ASTparam) children[i];

            /* figure out what the proper stack reference should be */
            int stackRef = children.length - i /* add 1 more to hop over PC */ + 1;
            SymTableEntry entry = (SymTableEntry) param.jjtAccept(this, stackRef);
            symTable.enterLocalParam(param.value.toString(), entry);
        }

        return symTable;
    }

    public Object visit(ASTparam node, Object data) {
        Node[] children = node.children;

        int stackRef = Integer.parseInt(data.toString());

        /* get the type "int" */
        String type = children[0].jjtAccept(this,data).toString();

        /* get the identifier */
        String identifier = node.value.toString();

        /* determine entry size */
        int size;
        if(children.length == 2) { /* this is an array param*/
            size = 0; /* we'll use 0 to indicate the unknown size of our array param */
        }
        else {
            size = 1;
        }

        /* create the entry */
        SymTableEntry entry = new SymTableEntry(type,"param",stackRef,size);

        return entry;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTarrayParamFlag node, Object data) {
        return node.childrenAccept(this,null);
    }

    public Object visit(ASTcompoundStatment node, Object data) {
        /* try and a declared symTable*/
        if (data != null) {
            symTableStack.pushLocal((SymTable) data);
        }
        else {
            symTableStack.pushLocal();
        }

        node.childrenAccept(this, null);

        /* clean up the function's stack usage */
        int numVars = symTableStack.getLocal().memUsage;
        out.printf("OPCODE(add) SRC1(R_sp) SRC2(%s) DEST(R_sp) // cleanup stack usage \n", numVars);

        symTableStack.popLocal();

        return data;
    }

    public Object visit(ASTlocalDeclarations node, Object data) {
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTstatementList node, Object data) {
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTstatement node, Object data) {
        return node.childrenAccept(this, null);
    }

    public Object visit(ASTexpressionStatement node, Object data) {
        return node.childrenAccept(this, null);
    }

    public Object visit(ASTassignStatement node, Object data) {
        Node[] children = node.children;

        /* generate the expression code */
        children[1].jjtAccept(this, data);

        String ref = children[0].jjtAccept(this, data).toString();
        out.printf("OPCODE(mov) SRC1(TOS) DEST(%s)\n", ref);

        return data;
    }

    public Object visit(ASTvar node, Object data) {
        Node[] children = node.children;

        /* lookup the id */
        String identifier = node.value.toString();
        SymTableEntry entry = symTableStack.lookup(identifier);

        /* determine the offset */
        int offset = 0;
        if (entry.size == 1) /* scalar */ {
            offset = entry.stackRef;
        } else /* array */ {
            if (children != null) {
                int arrRef = Integer.parseInt(children[0].jjtAccept(this, data).toString());
                offset = arrRef + entry.stackRef;
            } else {
                /* this was not declared as scalar */
                System.out.println("array variable cannot be accessed like a scalar variable...");
                out.println("array variable cannot be accessed like a scalar variable...");
            }
        }

        String ref = createRef(entry,offset);

        exprStack.push(ref);

        /* this is going to be the offset if it's an array,
            if not then the offset will just come back as zero.
         */
        return ref;
    }

    public Object visit(ASTreturnStatement node, Object data) {
        node.childrenAccept(this, null);

        out.printf("OPCODE(mov) SRC1(TOS) DEST(R_999)\n");

        return data;
    }

    public Object visit(ASTselectionStatement node, Object data) {
        Node[] children = node.children;

        int labelNum = symTableStack.getLabelNum();


        /* generate code for relational expression */
        Object relOpr = children[0].jjtAccept(this, null);  // relationalExpression()
        String relCMM = JumpCondition.findForOperator(relOpr.toString()).toString().toLowerCase();

        Object op2 = exprStack.pop();
        Object op1 = exprStack.pop();
        out.printf("OPCODE(%s) SRC1(%s) SRC2(%s) DEST(if_%s)\n", relCMM, op1, op2, labelNum);

        if (children.length > 2) {
            /* generate jmp to else clause */
            out.printf("OPCODE(jmp) DEST(else_%s)\n",  labelNum);
        } else {
            /* just jump to continue label */
            out.printf("OPCODE(jmp) DEST(cont_%s)\n",  labelNum);
        }

        /* create selection statement headers */
        out.printf("LABEL(if_%s) OPCODE(nop)\n", labelNum);

        /* generate statements for if true */
        children[1].jjtAccept(this, null);

        /* generate jmp to else clause */
        out.printf("OPCODE(jmp) DEST(cont_%s)\n", labelNum);

        if (children.length > 2) {
            /* generate statements else clause */
            out.printf("LABEL(else_%s) OPCODE(nop)\n", labelNum);
            children[2].jjtAccept(this, data);
        }

        /* continue label used in case of no else clause */
        out.printf("LABEL(cont_%s) OPCODE(nop)\n", labelNum);

        return data;
    }

    public Object visit(ASTiterationStatement node, Object data) {
        Node[] children = node.children;

        int labelNum = symTableStack.getLabelNum();

        /* generate while test header */
        out.printf("label(while_test_%s) opcode(nop)\n", labelNum);

        /* generate code for relational expression */
        Object relOpr = children[0].jjtAccept(this, null);  // relationalExpression()
        String relCMM = JumpCondition.findForOperator(relOpr.toString()).toString().toLowerCase();

        Object op2 = exprStack.pop();
        Object op1 = exprStack.pop();
        out.printf("OPCODE(%s) SRC1(%s) SRC2(%s) DEST(while_body_%s)\n", relCMM, op1, op2, labelNum);

        /* if the test fails jump to continue label */
        out.printf("OPCODE(jmp) DEST(cont_%s)\n",  labelNum);

        /* generate the while body */
        out.printf("LABEL(while_body_%s) OPCODE(nop)\n",  labelNum);
        children[1].jjtAccept(this, null);

        /* jump back to test after while body */
        out.printf("OPCODE(jmp) DEST(while_test_%s)\n", labelNum);

        /* continue label */
        out.printf("LABEL(cont_%s) OPCODE(nop)\n", labelNum);

        return data;
    }

    public Object visit(ASTrelationalExpression node, Object data) {
        Node[] children = node.children;
        children[2].jjtAccept(this, data);
        children[0].jjtAccept(this, data);

        return children[1].jjtAccept(this, data);
    }

    public Object visit(ASTrelop node, Object data) {
        return node.value;
    }

    public Object visit(ASTadditiveExpression node, Object data) {
        Node[] children = node.children;

        if (children.length == 1) {
            children[0].jjtAccept(this, data);
            Object op1 = exprStack.pop();
            if (!op1.toString().equals("TOS")) {
                out.printf("OPCODE(mov) SRC1(%s) DEST(TOS) // assign prep\n", op1);
            }
            exprStack.push("TOS");
            return data;
        } else {
            return node.childrenAccept(this, data);
        }
    }

    public Object visit(ASTadditiveExpressionPrime node, Object data) {
        Node[] children = node.children;

        Object value = children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);

        return data;
    }

    public Object visit(ASTaddop node, Object data) {

        String opr = node.value == "+" ? "sub" : "add";

        Object op2 = exprStack.pop();
        Object op1 = exprStack.pop();
        out.printf("OPCODE(%s) SRC1(%s) SRC2(%s) DEST(TOS) //TOS = %s %s %s\n", opr, op1, op2, op1, node.value, op2);
        exprStack.push("TOS");
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTterm node, Object data) {
        Node[] children = node.children;

        if (children.length == 1) {
            return children[0].jjtAccept(this, data);
        }

        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTtermPrime node, Object data) {
        Node[] children = node.children;

        Object value = children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);

        return value;
    }

    public Object visit(ASTmultop node, Object data) {

        String opr = node.value == "*" ? "div" : "mul";

        Object op2 = exprStack.pop();
        Object op1 = exprStack.pop();
        out.printf("OPCODE(%s) SRC1(%s) SRC2(%s) DEST(TOS) // TOS =  %s %s %s\n", opr, op1, op2, op1, node.value, op2);
        exprStack.push("TOS");
        return node.childrenAccept(this, null);  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object visit(ASTfactor node, Object data) {
        if (node.children == null) {
            SymTableEntry entry = symTableStack.lookup(node.value.toString());

            if (entry != null) {
                String ref = createRef(entry);
                exprStack.push(ref);
            } else {
                exprStack.push(node.value);
            }

            if (flag == CMMVisitor.Flag.CALL) {
                argsStack.push(node.value);
            }

            return node.value;
        } else {
            return node.children[0].jjtAccept(this, null);
        }
    }

    public Object visit(ASTcall node, Object data) {
        flag = Flag.CALL;

        String funcName = node.value.toString();

        int argsCount = Integer.parseInt(node.children[0].jjtAccept(this, data).toString());

        if (funcName.equals("print")) {
            generatePrint();
        }
        else {
            out.printf("OPCODE(call) DEST(%s)\n",funcName);

            /* cleanup args from stack */
            out.printf("OPCODE(add) SRC1(R_sp) SRC2(%s) DEST(R_sp) // cleanup stack usage for call \n", argsCount);

            /* move the return value to TOS */
            out.printf("OPCODE(mov) SRC1(R_999) DEST(TOS)\n");
        }

        flag = Flag.EXPRESSION;
        return data;
    }

    private void generatePrint() {
        out.printf("OPCODE(print) SRC1(TOS)\n");
    }

    public Object visit(ASTargs node, Object data) {
        node.childrenAccept(this, null);

        return node.children.length;
    }

    private String createRef(SymTableEntry entry) { return createRef(entry, 0); }
    private String createRef(SymTableEntry entry,int offset) {
        String spec = entry.spec;

        if(spec.equals("var")) {
            return String.format("[fp-%s]",entry.stackRef);
        }

        if(spec.equals("param")) {
            return String.format("[fp+%s]",entry.stackRef);
        }



        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    enum Flag {
        CALL, EXPRESSION;
    }


}

