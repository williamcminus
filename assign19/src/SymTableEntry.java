/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 4/26/11
 * Time: 12:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class SymTableEntry {
    public String type;
    public int stackRef;
    public int size;
    public String spec;


    public SymTableEntry(String type, String spec, int stackRef, int size) {
        this.type = type;
        this.stackRef = stackRef;
        this.size = size;
        this.spec = spec;
    }
}
