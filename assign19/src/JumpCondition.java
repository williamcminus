public enum JumpCondition {
    JEQ("=="),
    JLT("<"),
    JLE("<="),
    JNE("!="),
    JGE(">="),
    JGT(">");

    private String text;

    JumpCondition() {
        this.text = this.toString().toLowerCase();
    }

    JumpCondition(String text) {
        this.text = text;
    }

    public static JumpCondition findForOperator(String operator) {
        for(JumpCondition cond : values()) {
            if(cond.text.equals(operator)) {
                return cond;
            }
        }
        return null;
    }
}
