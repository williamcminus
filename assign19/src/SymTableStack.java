import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 4/25/11
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class SymTableStack extends Stack<SymTable> {
    private int currentLevel;

    private int labelNum;
    public static int currentStackPos;

    public SymTableStack() {

        super();

        currentLevel = 0;
        currentStackPos = 1;
        labelNum = 0;
    }

    public void pushLocal() {
        SymTable table = new SymTable();
        push(table);

        currentLevel++;
    }

    public void pushLocal(SymTable table) {
        push(table);

        currentLevel++;
    }

    public SymTable popLocal() {

        /* cleanup the stack position once we leave the frame */
        currentStackPos -= getLocal().memUsage;

        currentLevel--;
        return pop();
    }

    public SymTable getLocal() {
        return peek();
    }

    public SymTableEntry lookup(String key) {
        for(int i = 0; i < currentLevel; i++) {
            SymTable table = this.elementAt(i);

            SymTableEntry value = table.lookupLocal(key);

            if(value != null) {
                return value;
            }
        }

        return null;
    }

    public int getLabelNum() {
        return labelNum++;
    }
}
