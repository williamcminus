package wci.frontend.CMinus.tokens;

import wci.frontend.Source;
import wci.frontend.CMinus.CMinusErrorCode;
import wci.frontend.CMinus.CMinusToken;

import static wci.frontend.CMinus.CMinusTokenType.ERROR;

/**
 * <h1>CMinusErrorToken</h1>
 *
 * <p>CMinus error token.</p>
 */
public class CMinusErrorToken extends CMinusToken
{
    /**
     * Constructor.
     * @param source the source from where to fetch subsequent characters.
     * @param errorCode the error code.
     * @param tokenText the text of the erroneous token.
     * @throws Exception if an error occurred.
     */
    public CMinusErrorToken(Source source, CMinusErrorCode errorCode,
                            String tokenText)
        throws Exception
    {
        super(source);

        this.text = tokenText;
        this.type = ERROR;
        this.value = errorCode;
    }

    /**
     * Do nothing.  Do not consume any source characters.
     * @throws Exception if an error occurred.
     */
    protected void extract()
        throws Exception
    {
    }
}
