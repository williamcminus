package wci.frontend.CMinus;

import wci.frontend.*;
import wci.frontend.CMinus.CMinusErrorCode;
import wci.frontend.CMinus.CMinusErrorHandler;
import wci.message.Message;

import static wci.frontend.CMinus.CMinusErrorCode.IO_ERROR;
import static wci.frontend.CMinus.CMinusTokenType.ERROR;
import static wci.message.MessageType.PARSER_SUMMARY;
import static wci.message.MessageType.TOKEN;

/**
 * <h1>CMinusParserTD</h1>
 * <p/>
 * <p>The top-down CMinus parser.</p>
 */
public class CMinusParserTD extends Parser {
    protected static CMinusErrorHandler errorHandler = new CMinusErrorHandler();

    /**
     * Constructor.
     *
     * @param scanner the scanner to be used with this parser.
     */
    public CMinusParserTD(Scanner scanner) {
        super(scanner);
    }

    /**
     * Parse a CMinus source program and generate the symbol table
     * and the intermediate code.
     */
    public void parse()
            throws Exception {
        Token token;
        long startTime = System.currentTimeMillis();

        try {
            // Loop over each token until the end of file.
            while (!((token = nextToken()) instanceof EofToken)) {
                TokenType tokenType = token.getType();

                if (tokenType != ERROR) {

                    // Format each token.
                    sendMessage(new Message(TOKEN,
                            new Object[]{token.getLineNumber(),
                                    token.getPosition(),
                                    tokenType,
                                    token.getText(),
                                    token.getValue()}));
                } else {
                    errorHandler.flag(token, (CMinusErrorCode) token.getValue(),
                            this);
                }

            }

            // Send the parser summary message.
            float elapsedTime = (System.currentTimeMillis() - startTime) / 1000f;
            sendMessage(new Message(PARSER_SUMMARY,
                    new Number[]{token.getLineNumber(),
                            getErrorCount(),
                            elapsedTime}));
        } catch (java.io.IOException ex) {
            errorHandler.abortTranslation(IO_ERROR, this);
        }
    }

    /**
     * Return the number of syntax errors found by the parser.
     *
     * @return the error count.
     */
    public int getErrorCount() {
        return errorHandler.getErrorCount();
    }
}
