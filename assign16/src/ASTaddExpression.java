/* Generated By:JJTree: Do not edit this line. ASTaddExpression.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ASTaddExpression extends SimpleNode {
  public ASTaddExpression(int id) {
    super(id);
  }

  public ASTaddExpression(ExprParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(ExprParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=e991f1363d58f2decec8b9e6ee2f6f12 (do not edit this line) */
