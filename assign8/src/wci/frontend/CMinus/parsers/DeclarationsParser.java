package wci.frontend.CMinus.parsers;

import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.symtabimpl.DefinitionImpl;

import static wci.frontend.CMinus.CMinusTokenType.*;

/**
 * <h1>DeclarationsParser</h1>
 * <p/>
 * <p>Parse CMinus declarations.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class DeclarationsParser extends CMinusParserTD {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public DeclarationsParser(CMinusParserTD parent) {
        super(parent);
    }


    static final EnumSet<CMinusTokenType> DECLARATION_START_SET =
            EnumSet.of(INT,FLOAT,BOOLEAN,CHAR);

    static final EnumSet<CMinusTokenType> TYPE_SET =
            DECLARATION_START_SET.clone();

    static final EnumSet<CMinusTokenType> VAR_START_SET =
            TYPE_SET.clone();

    static {

    }

    static final EnumSet<CMinusTokenType> ROUTINE_START_SET =
            VAR_START_SET.clone();

    static {

    }


    /**
     * Parse declarations.
     * To be overridden by the specialized declarations parser subclasses.
     *
     * @param token the initial token.
     * @throws Exception if an error occurred.
     */
    public void parse(Token token)
            throws Exception {
        token = synchronize(DECLARATION_START_SET);

        if (DECLARATION_START_SET.contains(token.getType())) {
            VariableDeclarationsParser variableDeclarationsParser =
                    new VariableDeclarationsParser(this);
            variableDeclarationsParser.setDefinition(DefinitionImpl.VARIABLE);
            variableDeclarationsParser.parse(token);
        }

    }
}
