package wci.frontend.CMinus;

import wci.frontend.EofToken;
import wci.frontend.Scanner;
import wci.frontend.Source;
import wci.frontend.Token;
import wci.frontend.CMinus.CMinusTokenType;
import wci.frontend.CMinus.tokens.*;

import static wci.frontend.Source.EOF;
import static wci.frontend.CMinus.CMinusErrorCode.INVALID_CHARACTER;

/**
 * <h1>CMinusScanner</h1>
 * <p/>
 * <p>The CMinus scanner.</p>
 */
public class CMinusScanner extends Scanner {
    /**
     * Constructor
     *
     * @param source the source to be used with this scanner.
     */
    public CMinusScanner(Source source) {
        super(source);
    }

    /**
     * Extract and return the next CMinus token from the source.
     *
     * @return the next token.
     * @throws Exception if an error occurred.
     */
    protected Token extractToken()
            throws Exception {
        skipWhiteSpace();

        Token token;
        char currentChar = currentChar();

        // Construct the next token.  The current character determines the
        // token type.
        if (currentChar == EOF) {
            token = new EofToken(source);
        } else if (Character.isLetter(currentChar)) {
            token = new CMinusWordToken(source);
        } else if (Character.isDigit(currentChar)) {
            token = new CMinusNumberToken(source);
        } else if (CMinusTokenType.SPECIAL_SYMBOLS
                .containsKey(Character.toString(currentChar))) {
            token = new CMinusSpecialSymbolToken(source);
        } else {
            token = new CMinusErrorToken(source, INVALID_CHARACTER,
                    Character.toString(currentChar));
            nextChar();  // consume character
        }

        return token;
    }

    /**
     * Skip whitespace characters by consuming them.  A comment is whitespace.
     *
     * @throws Exception if an error occurred.
     */
    private void skipWhiteSpace()
            throws Exception {
        char currentChar = currentChar();

        while (Character.isWhitespace(currentChar) || (currentChar == '/' && source.peekChar() == '*')) {

            // Start of a comment?
            if (currentChar == '/') {
                // skip the opening '*'
                currentChar = nextChar();

                do {
                    currentChar = nextChar();  // consume comment characters
                } while ((currentChar != '*' && source.peekChar() != '/') && (currentChar != EOF));

                // skip the closing '*'
                currentChar = nextChar();

                // Found closing '}'?
                if (currentChar == '/') {
                    currentChar = nextChar();  // consume the '}'
                }
            }

            // Not a comment.
            else {
                currentChar = nextChar();  // consume whitespace character
            }
        }
    }
}
