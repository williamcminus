package wci.frontend.CMinus.parsers;

import java.util.ArrayList;
import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.icodeimpl.ICodeKeyImpl;
import wci.intermediate.icodeimpl.ICodeNodeTypeImpl;
import wci.intermediate.symtabimpl.DefinitionImpl;
import wci.intermediate.typeimpl.TypeFormImpl;

import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.intermediate.symtabimpl.RoutineCodeImpl.*;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.*;

/**
 * <h1>BlockDeclarationsParser</h1>
 * <p/>
 * <p>Parse CMinus declarations.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class DeclarationsParser extends CMinusParserTD {
    private Definition definition;  // how to define the identifier


    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public DeclarationsParser(CMinusParserTD parent) {
        super(parent);
    }

    /**
     * Setter.
     *
     * @param definition the definition to set.
     */
    protected void setDefinition(Definition definition) {
        this.definition = definition;
    }


    static final EnumSet<CMinusTokenType> DECLARATION_START_SET =
            EnumSet.of(INT, FLOAT, BOOLEAN, CHAR);

    static final EnumSet<CMinusTokenType> DECLARATION_DECIDE_SET =
            EnumSet.of(LEFT_BRACKET, LEFT_PAREN, SEMICOLON, COMMA, RIGHT_PAREN);

    static final EnumSet<CMinusTokenType> PARM_SET =
            EnumSet.of(COMMA, RIGHT_PAREN);

    static final EnumSet<CMinusTokenType> TYPE_SET =
            DECLARATION_START_SET.clone();

    static final EnumSet<CMinusTokenType> VAR_START_SET =
            TYPE_SET.clone();

    static {

    }

    static final EnumSet<CMinusTokenType> ROUTINE_START_SET =
            VAR_START_SET.clone();

    static {

    }


    /**
     * Parse declarations.
     * To be overridden by the specialized declarations parser subclasses.
     *
     * @param token the initial token.
     * @throws Exception if an error occurred.
     */
    public SymTabEntry parse(Token token, SymTabEntry parentId, boolean inBlock)
            throws Exception {

        if (DECLARATION_START_SET.contains(token.getType())) {
            while (DECLARATION_START_SET.contains(token.getType())) {
                // here we need to see if we have a function declaration or a variable declaration
                DeclarationHelpers declarationHelpers = new DeclarationHelpers(this);

                // get the function return type | variable type
                TypeSpec type = declarationHelpers.parseTypeSpec(token);

                token = nextToken(); // consume the type

                Token idToken = token;
                SymTabEntry identifierId = declarationHelpers.parseIdentifier(token);

                token = nextToken(); // consume the identifierId

                // sync to the '[' or '(' so we can decide if we have a variable or function
                token = synchronize(DECLARATION_DECIDE_SET);
                switch ((CMinusTokenType) token.getType()) {
                    case SEMICOLON:
                    case LEFT_BRACKET: { /* variable */
                        declarationHelpers.decideVariableDeclaration(identifierId, type, token);

                        // set definition to variable since its not in a parameter list
                        identifierId.setDefinition(DefinitionImpl.VARIABLE);
                        break;
                    }
                    case LEFT_PAREN: /* function dec */
                    default: {
                        if (inBlock) {
                            errorHandler.flag(token, FUNC_DEC_NOT_ALLOWED, this);
                        } else {
                            //TODO: reformat this out

                            // redefine the identifier as a function
                            SymTabEntry routineId = identifierId;
                            routineId.setDefinition(DefinitionImpl.FUNCTION);

                            // Create new intermediate code for the routine.
                            ICode iCode = ICodeFactory.createICode();
                            routineId.setAttribute(ROUTINE_ICODE, iCode);
                            routineId.setAttribute(ROUTINE_CODE, DECLARED);

                            // Push the routine's new symbol table onto the stack.
                            routineId.setAttribute(ROUTINE_SYMTAB, symTabStack.push());

                            // Append to the parent's list of routines.
                            ArrayList<SymTabEntry> subroutines = (ArrayList<SymTabEntry>)
                                    parentId.getAttribute(ROUTINE_ROUTINES);
                            subroutines.add(routineId);

                            /* add the return type */
                            // The return type cannot be an array or record.
                            if (type != null) {
                                TypeForm form = type.getForm();
                                if ((form == TypeFormImpl.ARRAY) ||
                                        (form == TypeFormImpl.RECORD)) {
                                    errorHandler.flag(token, INVALID_TYPE, this);
                                }
                            }
                            routineId.setTypeSpec(type);

                            // Parse the function's parameters
                            token = nextToken(); // consume the '('
                            ArrayList<SymTabEntry> parms = declarationHelpers.parseFunctionParams(token);
                            routineId.setAttribute(ROUTINE_PARMS, parms);

                            token = nextToken(); // consume the ')'

                            // parse the function's block
                            BlockParser blockParser = new BlockParser(this);
                            ICodeNode blockNode = blockParser.parse(token, routineId);

                            completeReturnNodes(blockNode, idToken);

                            iCode.setRoot(blockNode);

                            // Pop the routine's symbol table off the stack.
                            symTabStack.pop();
                        }
                    }
                }

                token = currentToken();
            }
        }

        return null;
    }

    // Adds the functions ICodeNode as a child of all RETURN nodes within that functions iCode tree
    private void completeReturnNodes(ICodeNode blockNode, Token idToken) throws Exception {
        for (ICodeNode child : blockNode.getChildren()) {
            ICodeNodeType childType = child.getType();

            if (childType == ICodeNodeTypeImpl.RETURN) {
                SymTabEntry functionId = symTabStack.lookup(idToken.getText().toLowerCase());

                ICodeNode functionNode = ICodeFactory.createICodeNode(ICodeNodeTypeImpl.VARIABLE);
                functionNode.setAttribute(ICodeKeyImpl.ID,functionId);

                TypeSpec functionType = functionId.getTypeSpec();
                functionNode.setTypeSpec(functionType);

                child.addChild(functionNode);
            } else {
                completeReturnNodes(child, idToken);
            }
        }
    }
}
