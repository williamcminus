package wci.frontend.CMinus.parsers;

import wci.frontend.CMinus.CMinusTokenType;
import wci.frontend.Token;
import wci.frontend.CMinus.CMinusParserTD;
import wci.intermediate.ICodeFactory;
import wci.intermediate.ICodeNode;
import wci.intermediate.SymTabEntry;

import java.util.EnumSet;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.intermediate.icodeimpl.ICodeKeyImpl.ID;
import static wci.intermediate.icodeimpl.ICodeNodeTypeImpl.ASSIGN;
import static wci.intermediate.icodeimpl.ICodeNodeTypeImpl.VARIABLE;

/**
 * <h1>AssignmentStatementParser</h1>
 *
 * <p>Parse a CMinus assignment statement.</p>
 */
public class AssignmentStatementParser extends StatementParser
{
    /**
     * Constructor.
     * @param parent the parent parser.
     */
    public AssignmentStatementParser(CMinusParserTD parent)
    {
        super(parent);
    }

    // Synchronization set for the := token.
    private static final EnumSet<CMinusTokenType> GETS_SET =
        ExpressionParser.EXPR_START_SET.clone();
    static {
        GETS_SET.add(GETS);
        GETS_SET.addAll(StatementParser.STMT_FOLLOW_SET);
    }

    /**
     * Parse an assignment statement.
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token)
        throws Exception
    {
        // Create the ASSIGN node.
        ICodeNode assignNode = ICodeFactory.createICodeNode(ASSIGN);

        // Look up the target identifer in the symbol table stack.
        // Enter the identifier into the table if it's not found.
        String targetName = token.getText().toLowerCase();
        SymTabEntry targetId = symTabStack.lookup(targetName);
        if (targetId == null) {
            targetId = symTabStack.enterLocal(targetName);
        }
        targetId.appendLineNumber(token.getLineNumber());

        token = nextToken();  // consume the identifier token

        // Create the variable node and set its name attribute.
        ICodeNode variableNode = ICodeFactory.createICodeNode(VARIABLE);
        variableNode.setAttribute(ID, targetId);

        // The ASSIGN node adopts the variable node as its first child.
        assignNode.addChild(variableNode);

        // Synchronize on the = token.
        token = synchronize(GETS_SET);
        if (token.getType() == GETS) {
            token = nextToken();  // consume the =
        }
        else {
            errorHandler.flag(token, MISSING_GETS, this);
        }

        // Parse the expression.  The ASSIGN node adopts the expression's
        // node as its second child.
        ExpressionParser expressionParser = new ExpressionParser(this);
        assignNode.addChild(expressionParser.parse(token));

        return assignNode;
    }
}
