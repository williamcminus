PROGRAM Tester;

VAR
   x	: integer;

FUNCTION xyz(x,y : integer) : integer;
     BEGIN
	   xyz :=   x+y;
     END;

BEGIN
   x := xyz(111,222);
   writeln(x);
END.