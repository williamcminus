.class public tester
.super java/lang/Object


.field private static status I

.method public <init>()V

	aload_0
	invokenonvirtual	java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

.method private static sum(II)I

.var 0 is x I
.var 1 is y I
.var 2 is sum I


.line 2
	iload_0
	iload_1
	iadd
	istore_2

	iload_2
	ireturn

.limit locals 3
.limit stack 2
.end method

.method private static run()I

.var 1 is run I


.line 5
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"%d\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	bipush	111
	sipush	222
	invokestatic	tester/sum(II)I
	invokestatic	java/lang/Integer.valueOf(I)Ljava/lang/Integer;
	aastore
	invokestatic	java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
	invokevirtual	java/io/PrintStream.print(Ljava/lang/String;)V
.line 6
	iconst_0
	istore_1

	iload_1
	ireturn

.limit locals 2
.limit stack 8
.end method

.method public static main([Ljava/lang/String;)V



.line 10
	invokestatic	tester/run()I
	putstatic	tester/status I


	return

.limit locals 1
.limit stack 3
.end method
