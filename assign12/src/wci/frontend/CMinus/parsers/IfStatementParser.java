package wci.frontend.CMinus.parsers;

import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.icodeimpl.*;
import wci.intermediate.symtabimpl.Predefined;
import wci.intermediate.symtabimpl.Predefined;
import wci.intermediate.typeimpl.CMinusTypeChecker;

import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.frontend.CMinus.CMinusErrorCode.*;

/**
 * <h1>IfStatementParser</h1>
 *
 * <p>Parse a CMinus IF statement.</p>
 */
public class IfStatementParser extends StatementParser
{
    /**
     * Constructor.
     * @param parent the parent parser.
     */
    public IfStatementParser(CMinusParserTD parent)
    {
        super(parent);
    }

    // Synchronization set for THEN.
    private static final EnumSet<CMinusTokenType> IF_SET =
        ExpressionParser.EXPR_START_SET.clone();

    // Synchronization set for THEN.
    private static final EnumSet<CMinusTokenType> THEN_SET =
        StatementParser.STMT_START_SET.clone();
    static {
        THEN_SET.add(RIGHT_PAREN);
    }

    /**
     * Parse an IF statement.
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token)
        throws Exception
    {
        token = nextToken();  // consume the IF

        // sync at the '('
        token = synchronize(IF_SET);
        if (token.getType() == LEFT_PAREN) {
            token = nextToken();  // consume the '('
        }
        else {
            errorHandler.flag(token, MISSING_LEFT_PAREN, this);
        }

        // Create an IF node.
        ICodeNode ifNode = ICodeFactory.createICodeNode(ICodeNodeTypeImpl.IF);

        // Parse the expression.
        // The IF node adopts the expression subtree as its first child.
        ExpressionParser expressionParser = new ExpressionParser(this);
        ICodeNode exprNode = expressionParser.parse(token);
        ifNode.addChild(exprNode);

        // Type check: The expression type must be boolean.
        TypeSpec exprType = exprNode != null ? exprNode.getTypeSpec()
                                             : Predefined.undefinedType;
        if (!CMinusTypeChecker.isBoolean(exprType)) {
            errorHandler.flag(token, INCOMPATIBLE_TYPES, this);
        }

        //Synchronize at the ')'
        token = synchronize(THEN_SET);
        if (token.getType() == RIGHT_PAREN) {
            token = nextToken();  // consume the ')'
        }
        else {
            errorHandler.flag(token, MISSING_RIGHT_PAREN, this);
        }

        // Parse the true statement.
        // The IF node adopts the statement subtree as its second child.
        StatementParser statementParser = new StatementParser(this);
        ifNode.addChild(statementParser.parse(token));
        token = currentToken();

        // case where we just have one statement
        if (token.getType() == SEMICOLON) {
            token = nextToken(); // consume the ';'
        }

        // Look for an ELSE.
        if (token.getType() == ELSE) {
            token = nextToken();  // consume the ELSE

            // Parse the ELSE statement.
            // The IF node adopts the statement subtree as its third child.
            ifNode.addChild(statementParser.parse(token));
        }

        return ifNode;
    }
}
