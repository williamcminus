package wci.frontend.CMinus.parsers;

import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.symtabimpl.Predefined;
import wci.intermediate.symtabimpl.Predefined;

import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.intermediate.typeimpl.TypeFormImpl.ARRAY;
import static wci.intermediate.typeimpl.TypeFormImpl.SUBRANGE;
import static wci.intermediate.typeimpl.TypeKeyImpl.*;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 2/27/11
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ArrayTypeParser extends TypeSpecificationParser {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    protected ArrayTypeParser(CMinusParserTD parent) {
        super(parent);
    }

    public TypeSpec parse(Token token) throws Exception {
        TypeSpec arrayType = TypeFactory.createType(ARRAY);

        token = nextToken(); // get the element count

        TypeSpec subrangeType = TypeFactory.createType(SUBRANGE);
        arrayType.setAttribute(ARRAY_INDEX_TYPE, subrangeType);
        subrangeType.setAttribute(SUBRANGE_BASE_TYPE, Predefined.integerType);

        if (token.getType() == INTEGER) {
            subrangeType.setAttribute(SUBRANGE_MIN_VALUE, 0);
            subrangeType.setAttribute(SUBRANGE_MAX_VALUE, token.getValue());

            int count = ((Integer) token.getValue()) + 1;
            arrayType.setAttribute(ARRAY_ELEMENT_COUNT, count);

            token = nextToken();

            // Synchronize at the ] token.
            token = synchronize(EnumSet.of(RIGHT_BRACKET));
        } else if (token.getType() == RIGHT_BRACKET) {
            subrangeType.setAttribute(SUBRANGE_MIN_VALUE, 0);
            subrangeType.setAttribute(SUBRANGE_MAX_VALUE, -1);

            arrayType.setAttribute(ARRAY_ELEMENT_COUNT, -1);
        } else {
            errorHandler.flag(token, CMinusErrorCode.UNEXPECTED_TOKEN, this);
        }


        if (token.getType() == RIGHT_BRACKET) {
            token = nextToken();  // consume [
        } else {
            errorHandler.flag(token, MISSING_RIGHT_BRACKET, this);
        }

        return arrayType;
    }

}
