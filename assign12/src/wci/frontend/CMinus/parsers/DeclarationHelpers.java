package wci.frontend.CMinus.parsers;

import wci.frontend.CMinus.CMinusParserTD;
import wci.frontend.CMinus.CMinusTokenType;
import wci.frontend.Token;
import wci.frontend.TokenType;
import wci.intermediate.Definition;
import wci.intermediate.SymTabEntry;
import wci.intermediate.TypeSpec;
import wci.intermediate.symtabimpl.DefinitionImpl;

import java.util.ArrayList;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.SLOT;
import static wci.intermediate.typeimpl.TypeKeyImpl.ARRAY_ELEMENT_TYPE;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 3/2/11
 * Time: 1:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class DeclarationHelpers extends DeclarationsParser {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public DeclarationHelpers(CMinusParserTD parent) {
        super(parent);
    }

    /**
     * Parse the type specification.
     *
     * @param token the current token.
     * @return the type specification.
     * @throws Exception if an error occurs.
     */
    public TypeSpec parseTypeSpec(Token token)
            throws Exception {

        // Parse the type specification.
        TypeSpecificationParser typeSpecificationParser =
                new TypeSpecificationParser(this);
        TypeSpec type = typeSpecificationParser.parse(token);

        return type;
    }

    /**
     * Parse an identifier.
     *
     * @param token the current token.
     * @return the symbol table entry of the identifier.
     * @throws Exception if an error occurred.
     */
    public SymTabEntry parseIdentifier(Token token)
            throws Exception {
        SymTabEntry id = null;

        if (token.getType() == IDENTIFIER) {
            String name = token.getText().toLowerCase();
            id = symTabStack.lookupLocal(name);

            // Enter a new identifier into the symbol table.
            if (id == null) {
                id = symTabStack.enterLocal(name);
                id.appendLineNumber(token.getLineNumber());

                // Set its slot number in the local variables array.
                int slot = id.getSymTab().nextSlotNumber();
                id.setAttribute(SLOT, slot);
            } else {
                errorHandler.flag(token, IDENTIFIER_REDEFINED, this);
            }
        } else {
            errorHandler.flag(token, MISSING_IDENTIFIER, this);
        }

        return id;
    }

    public ArrayList<SymTabEntry> parseFunctionParams(Token token) throws Exception {
        ArrayList<SymTabEntry> sublist = new ArrayList<SymTabEntry>();

        while (DECLARATION_START_SET.contains(token.getType())) {
            token = synchronize(DECLARATION_START_SET);

            // get the function return type | variable type
            TypeSpec type = parseTypeSpec(token);

            token = nextToken(); // consume the type

            SymTabEntry identifierId = parseIdentifier(token);

            token = nextToken(); // consume the identifier

            decideVariableDeclaration(identifierId, type, token);

            if (identifierId != null) {
                sublist.add(identifierId);
            }

            token = synchronize(PARM_SET);
            TokenType tokenType = token.getType();

            // Look for the comma.
            if (tokenType == COMMA) {
                token = nextToken();  // consume the comma
            }


        }

        return sublist;
    }


    public void decideVariableDeclaration(SymTabEntry identifierId, TypeSpec type, Token token) throws Exception {
        token = synchronize(DECLARATION_DECIDE_SET);

        switch ((CMinusTokenType) token.getType()) {
            case LEFT_BRACKET: { /* array variable */
                // function parameters by default, this will get changed by caller if variable
                identifierId.setDefinition(DefinitionImpl.VAR_PARM);

                // we need to change the type spec of our sym table entry to array
                TypeSpec arrayType = parseTypeSpec(token);

                arrayType.setAttribute(ARRAY_ELEMENT_TYPE, type);

                identifierId.setTypeSpec(arrayType);

                token = currentToken();

                // Look for one or more semicolons after a definition.
                while (token.getType() == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
                break;
            }
            case COMMA:
            case RIGHT_PAREN:
            case SEMICOLON: { /* scalar variable */
                Definition def;
                // function parameters by default, this will get changed by caller if variable
                identifierId.setDefinition(DefinitionImpl.VALUE_PARM);

                identifierId.setTypeSpec(type);

                // Look for one or more semicolons after a definition.
                while (token.getType() == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
                break;
            }
            default: {
                errorHandler.flag(token, UNEXPECTED_TOKEN, this);
            }
        }
    }
}
