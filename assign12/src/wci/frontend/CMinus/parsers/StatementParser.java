package wci.frontend.CMinus.parsers;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import wci.frontend.EofToken;
import wci.frontend.Token;
import wci.frontend.TokenType;
import wci.frontend.CMinus.CMinusErrorCode;
import wci.frontend.CMinus.CMinusParserTD;
import wci.frontend.CMinus.CMinusTokenType;
import wci.intermediate.Definition;
import wci.intermediate.ICodeFactory;
import wci.intermediate.ICodeNode;
import wci.intermediate.SymTabEntry;
import wci.intermediate.symtabimpl.DefinitionImpl;

import java.util.EnumSet;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.intermediate.icodeimpl.ICodeKeyImpl.LINE;
import static wci.intermediate.icodeimpl.ICodeNodeTypeImpl.NO_OP;
import static wci.intermediate.symtabimpl.DefinitionImpl.UNDEFINED;

/**
 * <h1>StatementParser</h1>
 * <p/>
 * <p>Parse a CMinus statement.</p>
 */
public class StatementParser extends CMinusParserTD {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public StatementParser(CMinusParserTD parent) {
        super(parent);
    }

    // Synchronization set for starting a statement.
    protected static final EnumSet<CMinusTokenType> STMT_START_SET =
            EnumSet.of(LEFT_BRACE, IF, WHILE, IDENTIFIER, SEMICOLON, RETURN);

    // Synchronization set for following a statement.
    protected static final EnumSet<CMinusTokenType> STMT_FOLLOW_SET =
            EnumSet.of(RIGHT_BRACE, SEMICOLON, ELSE);

    // Synchronization set for following a statement.
    protected static final EnumSet<CMinusTokenType> STMT_SET =
            STMT_START_SET.clone();

    static {
        STMT_SET.addAll(STMT_FOLLOW_SET);
    }


    /**
     * Parse a statement.
     * To be overridden by the specialized statement parser subclasses.
     *
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token)
            throws Exception {
        ICodeNode statementNode = null;

        switch ((CMinusTokenType) token.getType()) {

            case LEFT_BRACE: {
                BlockParser blockParser =
                        new BlockParser(this);
                statementNode = blockParser.parse(token, super.getRoutineId());
                break;
            }

            // An assignment statement begins with a variable's identifier.
            case IDENTIFIER: {
                String name = token.getText().toLowerCase();
                SymTabEntry id = symTabStack.lookup(name);
                Definition idDefn = id != null ? id.getDefinition()
                        : UNDEFINED;

                // Assignment statement or procedure call.
                switch ((DefinitionImpl) idDefn) {

                    case VARIABLE:
                    case VALUE_PARM:
                    case UNDEFINED: {
                        AssignmentStatementParser assignmentParser =
                                new AssignmentStatementParser(this);
                        statementNode = assignmentParser.parse(token);
                        break;
                    }

                    case FUNCTION: {
                        CallParser callParser = new CallParser(this);
                        statementNode = callParser.parse(token);
                        break;
                    }

                    case PROCEDURE: {
                        CallParser callParser = new CallParser(this);
                        statementNode = callParser.parse(token);
                        break;
                    }

                    default: {
                        errorHandler.flag(token, UNEXPECTED_TOKEN, this);
                        token = nextToken();  // consume identifier
                    }
                }

                break;
            }

            case IF: {
                IfStatementParser ifStatementParser =
                        new IfStatementParser(this);
                statementNode = ifStatementParser.parse(token);
                break;
            }

            case WHILE: {
                WhileStatementParser whileStatementParser =
                        new WhileStatementParser(this);
                statementNode = whileStatementParser.parse(token);
                break;
            }

            case RETURN: {
                ReturnStatementParser returnStatementParser =
                        new ReturnStatementParser(this);
                statementNode = returnStatementParser.parse(token);
                break;
            }

            default: {
                statementNode = ICodeFactory.createICodeNode(NO_OP);
                break;
            }
        }

        // Set the current line number as an attribute.
        setLineNumber(statementNode, token);

        return statementNode;
    }

    /**
     * Set the current line number as a statement node attribute.
     *
     * @param node  ICodeNode
     * @param token Token
     */
    protected void setLineNumber(ICodeNode node, Token token) {
        if (node != null) {
            node.setAttribute(LINE, token.getLineNumber());
        }
    }

    /**
     * Parse a statement list.
     *
     * @param token      the curent token.
     * @param parentNode the parent node of the statement list.
     * @param terminator the token type of the node that terminates the list.
     * @param errorCode  the error code if the terminator token is missing.
     * @throws Exception if an error occurred.
     */
    protected void parseList(Token token, ICodeNode parentNode,
                             CMinusTokenType terminator,
                             CMinusErrorCode errorCode)
            throws Exception {
        // Synchronization set for the terminator.
        EnumSet<CMinusTokenType> terminatorSet = STMT_START_SET.clone();
        terminatorSet.add(terminator);

        // set of token types which start a statement requiring semicolon at end
        EnumSet<CMinusTokenType> semiSet = EnumSet.of(IDENTIFIER, RETURN);

        boolean semiRequired = false;

        // Loop to parse each statement until the END token
        // or the end of the source file.
        while (!(token instanceof EofToken) &&
                (token.getType() != terminator)) {

            // see if we need a semi colon to end this statement
            if (semiSet.contains(token.getType()))
                semiRequired = true;
            else
                semiRequired = false;

            // Parse a statement.  The parent node adopts the statement node.
            ICodeNode statementNode = parse(token);
            parentNode.addChild(statementNode);

            token = currentToken();
            TokenType tokenType = token.getType();

            // Look for the semicolon between statements if required.
            if (semiRequired) {
                if (tokenType == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
                // If at the start of the next assignment statement,
                // then missing a semicolon.
                else if (STMT_START_SET.contains(tokenType)) {
                    errorHandler.flag(token, MISSING_SEMICOLON, this);
                }
            } else {
                // look for the no op semi colon
                if (tokenType == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
            }

            // Synchronize at the start of the next statement
            // or at the terminator.
            token = synchronize(terminatorSet);
        }

        // Look for the terminator token.
        if (token.getType() == terminator) {
            token = nextToken();  // consume the terminator token
        } else {
            errorHandler.flag(token, errorCode, this);
        }
    }
}
