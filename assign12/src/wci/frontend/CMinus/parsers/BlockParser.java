package wci.frontend.CMinus.parsers;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;

/**
 * <h1>BlockParser</h1>
 * <p/>
 * <p>Parse a CMinus block.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class BlockParser extends CMinusParserTD {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public BlockParser(CMinusParserTD parent) {
        super(parent);
    }

    /**
     * Parse a block.
     *
     * @param token     the initial token.
     * @param routineId the symbol table entry of the routine name.
     * @return the root node of the parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token, SymTabEntry routineId)
            throws Exception {
        token = nextToken(); // consume the '{'
        DeclarationsParser declarationsParser = new DeclarationsParser(this);
        CompoundStatementParser compoundStatementParser = new CompoundStatementParser(this);

        // Parse any declarations.
        declarationsParser.parse(token,routineId,true);

        ICodeNode rootNode = null;

        token = currentToken();

        token = synchronize(StatementParser.STMT_SET);

        rootNode = compoundStatementParser.parse(token);

        return rootNode;
    }
}
