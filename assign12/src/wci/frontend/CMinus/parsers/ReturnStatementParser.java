package wci.frontend.CMinus.parsers;

import wci.frontend.CMinus.CMinusParserTD;
import wci.frontend.Token;
import wci.intermediate.ICodeFactory;
import wci.intermediate.ICodeNode;
import wci.intermediate.icodeimpl.ICodeNodeTypeImpl;

import static wci.intermediate.icodeimpl.ICodeNodeTypeImpl.*;

/**
 * Created by IntelliJ IDEA.
 * User: william
 * Date: 3/2/11
 * Time: 10:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReturnStatementParser extends StatementParser {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public ReturnStatementParser(CMinusParserTD parent) {
        super(parent);
    }

    /**
     * Parse an assignment statement.
     *
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token)
            throws Exception {
        // Create the return node.
        ICodeNode returnNode = ICodeFactory.createICodeNode(RETURN);

        // consume the 'return'
        token = nextToken();

        // we now look for an expression to return
        token = synchronize(ExpressionParser.EXPR_START_SET);

        ExpressionParser expressionParser = new ExpressionParser(this);
        ICodeNode expressionNode = expressionParser.parse(token);

        returnNode.addChild(expressionNode);

        return returnNode;
    }
}
