/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 4/11/11
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PostfixVisitor implements ExprParserVisitor {

    public Object visit(SimpleNode node, Object data) {
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTinfix node, Object data) {
        return node.childrenAccept(this, data);

    }

    public Object visit(ASTexpression node, Object data) {
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTexpressionPrime node, Object data) {
        Node[] children = node.children;
        children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);
        return data;
    }

    public Object visit(ASTaddop node, Object data) {
        if (node.value != null) {
            System.out.printf("%s ", node.value);
        }
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTterm node, Object data) {

        return node.childrenAccept(this, data);
    }

    public Object visit(ASTtermPrime node, Object data) {
        Node[] children = node.children;
        children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);
        return data;
    }

    public Object visit(ASTmultop node, Object data) {
        if (node.value != null) {
            System.out.printf("%s ", node.value);
        }
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTfactor node, Object data) {
        if (node.value != null) {
            System.out.printf("%s ", node.value);
        }
        return node.childrenAccept(this, data);
    }
}
