import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: weskinne
 * Date: 4/11/11
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class CodeGenVisitor implements ExprParserVisitor {
    private Stack<String> stack;

    public CodeGenVisitor() {
        stack = new Stack<String>();
    }

    public Object visit(SimpleNode node, Object data) {
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTinfix node, Object data) {
        return node.childrenAccept(this, data);

    }

    public Object visit(ASTexpression node, Object data) {
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTexpressionPrime node, Object data) {
        Node[] children = node.children;
        children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);
        return data;
    }

    public Object visit(ASTaddop node, Object data) {
        String op2 = stack.pop();
        String op1 = stack.pop();
        if (node.value != null) {
            String opcode = node.value.toString();
            if(opcode == "+") {
                opcode = "add";
            }
            else if(opcode == "-") {
                opcode = "sub";
            }

            System.out.printf("opcode(%s) src1(%s) src2(%s) dest(TOS)\n",
                    opcode,op1,op2);
            stack.push("TOS");
        }
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTterm node, Object data) {

        return node.childrenAccept(this, data);
    }

    public Object visit(ASTtermPrime node, Object data) {
        Node[] children = node.children;
        children[1].jjtAccept(this, data);
        children[0].jjtAccept(this, data);
        return data;
    }

    public Object visit(ASTmultop node, Object data) {
        String op2 = stack.pop();
        String op1 = stack.pop();
        if (node.value != null) {
            String opcode = node.value.toString();
            if(opcode == "/") {
                opcode = "div";
            }
            else if(opcode == "*") {
                opcode = "mul";
            }

            System.out.printf("opcode(%s) src1(%s) src2(%s) dest(TOS)\n",
                    opcode,op1,op2);
            stack.push("TOS");
        }
        return node.childrenAccept(this, data);
    }

    public Object visit(ASTfactor node, Object data) {
        if (node.value != null) {
            stack.push(node.value.toString());
        }
        return node.childrenAccept(this, data);
    }
}
