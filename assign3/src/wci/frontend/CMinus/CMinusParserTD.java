package wci.frontend.CMinus;

import wci.frontend.*;
import wci.frontend.CMinus.CMinusErrorCode;
import wci.frontend.CMinus.CMinusErrorHandler;
import wci.frontend.CMinus.parsers.StatementParser;
import wci.intermediate.ICodeFactory;
import wci.intermediate.ICodeNode;
import wci.intermediate.SymTabEntry;
import wci.message.Message;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.frontend.CMinus.CMinusTokenType.*;

import static wci.message.MessageType.PARSER_SUMMARY;
import static wci.message.MessageType.TOKEN;

/**
 * <h1>CMinusParserTD</h1>
 * <p/>
 * <p>The top-down CMinus parser.</p>
 */
public class CMinusParserTD extends Parser {
    protected static CMinusErrorHandler errorHandler = new CMinusErrorHandler();

    /**
     * Constructor.
     *
     * @param scanner the scanner to be used with this parser.
     */
    public CMinusParserTD(Scanner scanner) {
        super(scanner);
    }

    /**
     * Constructor for subclasses.
     * @param parent the parent parser.
     */
    public CMinusParserTD(CMinusParserTD parent)
    {
        super(parent.getScanner());
    }

    /**
     * Parse a CMinus source program and generate the symbol table
     * and the intermediate code.
     */
    public void parse()
        throws Exception
    {
        long startTime = System.currentTimeMillis();
        iCode = ICodeFactory.createICode();

        try {
            Token token = nextToken();
            ICodeNode rootNode = null;

            // Look for the BEGIN token to parse a compound statement.
            if (token.getType() == LEFT_BRACE) {
                StatementParser statementParser = new StatementParser(this);
                rootNode = statementParser.parse(token);
                token = currentToken();
            }
            else {
                errorHandler.flag(token, UNEXPECTED_TOKEN, this);
            }

            // Set the parse tree root node.
            if (rootNode != null) {
                iCode.setRoot(rootNode);
            }

            // Send the parser summary message.
            float elapsedTime = (System.currentTimeMillis() - startTime)/1000f;
            sendMessage(new Message(PARSER_SUMMARY,
                                    new Number[] {token.getLineNumber(),
                                                  getErrorCount(),
                                                  elapsedTime}));
        }
        catch (java.io.IOException ex) {
            errorHandler.abortTranslation(IO_ERROR, this);
        }
    }

    /**
     * Return the number of syntax errors found by the parser.
     *
     * @return the error count.
     */
    public int getErrorCount() {
        return errorHandler.getErrorCount();
    }
}
