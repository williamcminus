package wci.frontend.CMinus.tokens;

import wci.frontend.Source;
import wci.frontend.CMinus.CMinusToken;
import wci.frontend.CMinus.CMinusTokenType;

import static wci.frontend.CMinus.CMinusTokenType.IDENTIFIER;
import static wci.frontend.CMinus.CMinusTokenType.RESERVED_WORDS;

/**
 * <h1>CMinusWordToken</h1>
 *
 * <p> CMinus word tokens (identifiers and reserved words).</p>
 */
public class CMinusWordToken extends CMinusToken
{
    /**
     * Constructor.
     * @param source the source from where to fetch the token's characters.
     * @throws Exception if an error occurred.
     */
    public CMinusWordToken(Source source)
        throws Exception
    {
        super(source);
    }

    /**
     * Extract a CMinus word token from the source.
     * @throws Exception if an error occurred.
     */
    protected void extract()
        throws Exception
    {
        StringBuilder textBuffer = new StringBuilder();
        char currentChar = currentChar();

        // Get the word characters (letter or digit).  The scanner has
        // already determined that the first character is a letter.
        while (Character.isLetterOrDigit(currentChar)) {
            textBuffer.append(currentChar);
            currentChar = nextChar();  // consume character
        }

        text = textBuffer.toString();

        // Is it a reserved word or an identifier?
        type = (RESERVED_WORDS.contains(text))
               ? CMinusTokenType.valueOf(text.toUpperCase())  // reserved word
               : IDENTIFIER;                                  // identifier
    }
}
