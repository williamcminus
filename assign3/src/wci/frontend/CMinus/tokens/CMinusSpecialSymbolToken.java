package wci.frontend.CMinus.tokens;

import wci.frontend.Source;
import wci.frontend.CMinus.CMinusToken;

import static wci.frontend.CMinus.CMinusErrorCode.INVALID_CHARACTER;
import static wci.frontend.CMinus.CMinusTokenType.ERROR;
import static wci.frontend.CMinus.CMinusTokenType.SPECIAL_SYMBOLS;

/**
 * <h1>CMinusSpecialSymbolToken</h1>
 *
 * <p> CMinus special symbol tokens.</p>
 */
public class CMinusSpecialSymbolToken extends CMinusToken
{
    /**
     * Constructor.
     * @param source the source from where to fetch the token's characters.
     * @throws Exception if an error occurred.
     */
    public CMinusSpecialSymbolToken(Source source)
        throws Exception
    {
        super(source);
    }

    /**
     * Extract a CMinus special symbol token from the source.
     * @throws Exception if an error occurred.
     */
    protected void extract()
        throws Exception
    {
        char currentChar = currentChar();

        text = Character.toString(currentChar);
        type = null;

        switch (currentChar) {

            // Single-character special symbols.
            case '+':  case '-':  case '*':  case '/':  case ',':
            case ';':  case '(':  case ')':
            case '[':  case ']':  case '{':  case '}':  {
                nextChar();  // consume character
                break;
            }

            // = or ==
            case '=': {
                currentChar = nextChar();  // consume '=';

                if (currentChar == '=') {
                    text += currentChar;
                    nextChar();  // consume '='
                }

                break;
            }

            // < or <=
            case '<': {
                currentChar = nextChar();  // consume '<';

                if (currentChar == '=') {
                    text += currentChar;
                    nextChar();  // consume '='
                }

                break;
            }

            // > or >=
            case '>': {
                currentChar = nextChar();  // consume '>';

                if (currentChar == '=') {
                    text += currentChar;
                    nextChar();  // consume '='
                }

                break;
            }

            default: {
                nextChar();  // consume bad character
                type = ERROR;
                value = INVALID_CHARACTER;
            }
        }

        // Set the type if it wasn't an error.
        if (type == null) {
            type = SPECIAL_SYMBOLS.get(text);
        }
    }
}
