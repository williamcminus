package wci.frontend.CMinus.parsers;

import wci.frontend.CMinus.CMinusParserTD;
import wci.frontend.CMinus.CMinusTokenType;
import wci.frontend.Token;
import wci.intermediate.Definition;
import wci.intermediate.SymTabEntry;
import wci.intermediate.TypeSpec;

import java.util.EnumSet;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.intermediate.typeimpl.TypeKeyImpl.ARRAY_ELEMENT_TYPE;

/**
 * <h1>VariableDeclarationParser</h1>
 * <p/>
 * <p>Parse CMinus variable declarations.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class VariableDeclarationParser extends DeclarationsParser {
    private Definition definition;  // how to define the identifier

    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public VariableDeclarationParser(CMinusParserTD parent) {
        super(parent);
    }

    /**
     * Setter.
     *
     * @param definition the definition to set.
     */
    protected void setDefinition(Definition definition) {
        this.definition = definition;
    }

    // Synchronization set for a variable identifier.
    static final EnumSet<CMinusTokenType> VARIABLE_DECLARATION_SET =
            DeclarationsParser.DECLARATION_START_SET.clone();

    static {
        VARIABLE_DECLARATION_SET.add(LEFT_BRACKET);
        VARIABLE_DECLARATION_SET.add(SEMICOLON);
    }

    /**
     * Parse variable declarations.
     *
     * @param token the initial token.
     * @throws Exception if an error occurred.
     */
    public SymTabEntry parse(Token token)
            throws Exception {
        token = synchronize(VARIABLE_DECLARATION_SET);

        // Loop to parse a sequence of variable declarations
        // separated by semicolons.
        if (DeclarationsParser.DECLARATION_START_SET.contains(token.getType())) {

            Token typeToken = token;
            // get the type spec from the type identifier, such as 'int' or 'float', see CMinusPredefined
            TypeSpec type = parseTypeSpec(typeToken);

            token = synchronize(EnumSet.of(IDENTIFIER));

            Token identifierToken = token;
            SymTabEntry id = parseIdentifier(identifierToken);

            token = nextToken(); // consume the identifier

            if (token.getType() == LEFT_BRACKET) {

                TypeSpec arrayType = parseTypeSpec(token);

                arrayType.setAttribute(ARRAY_ELEMENT_TYPE, type);

                id.setTypeSpec(arrayType);

                token = currentToken();

                // Look for one or more semicolons after a definition.
                while (token.getType() == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
            } else if (token.getType() == SEMICOLON) {

                // the type spec we got is right
                id.setTypeSpec(type);

                // Look for one or more semicolons after a definition.
                while (token.getType() == SEMICOLON) {
                    token = nextToken();  // consume the ;
                }
            } else if (TYPE_SET.contains(token.getType())) {
                // If at the start of the next definition or declaration,
                // then missing a semicolon.
                errorHandler.flag(token, MISSING_SEMICOLON, this);
                // reset the loop
            }
        }

        return null;
    }


    /**
     * Parse an identifier.
     *
     * @param token the current token.
     * @return the symbol table entry of the identifier.
     * @throws Exception if an error occurred.
     */
    private SymTabEntry parseIdentifier(Token token)
            throws Exception {
        SymTabEntry id = null;

        if (token.getType() == IDENTIFIER) {
            String name = token.getText().toLowerCase();
            id = symTabStack.lookupLocal(name);

            // Enter a new identifier into the symbol table.
            if (id == null) {
                id = symTabStack.enterLocal(name);
                id.setDefinition(definition);
                id.appendLineNumber(token.getLineNumber());
            } else {
                errorHandler.flag(token, IDENTIFIER_REDEFINED, this);
            }
        } else {
            errorHandler.flag(token, MISSING_IDENTIFIER, this);
        }

        return id;
    }

    /**
     * Parse the type specification.
     *
     * @param token the current token.
     * @return the type specification.
     * @throws Exception if an error occurs.
     */
    protected TypeSpec parseTypeSpec(Token token)
            throws Exception {

        // Parse the type specification.
        TypeSpecificationParser typeSpecificationParser =
                new TypeSpecificationParser(this);
        TypeSpec type = typeSpecificationParser.parse(token);

        return type;
    }
}
