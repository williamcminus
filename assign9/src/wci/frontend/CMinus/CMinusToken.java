package wci.frontend.CMinus;

import wci.frontend.Source;
import wci.frontend.Token;

/**
 * <h1>CMinusToken</h1>
 *
 * <p>Base class for CMinus token classes.</p>
 */
public class CMinusToken extends Token
{
    /**
     * Constructor.
     * @param source the source from where to fetch the token's characters.
     * @throws Exception if an error occurred.
     */
    protected CMinusToken(Source source)
        throws Exception
    {
        super(source);
    }
}
