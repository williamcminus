package wci.frontend.CMinus;

import wci.frontend.CMinus.parsers.BlockParser;
import wci.frontend.EofToken;
import wci.frontend.Parser;
import wci.frontend.Scanner;
import wci.frontend.Token;
import wci.frontend.pascal.parsers.ProgramParser;
import wci.intermediate.ICode;
import wci.intermediate.ICodeFactory;
import wci.intermediate.ICodeNode;
import wci.intermediate.SymTabEntry;
import wci.intermediate.symtabimpl.CMinusPredefined;
import wci.intermediate.symtabimpl.DefinitionImpl;
import wci.intermediate.symtabimpl.Predefined;
import wci.message.Message;

import java.util.EnumSet;

import wci.frontend.CMinus.parsers.*;
import static wci.frontend.CMinus.CMinusErrorCode.IO_ERROR;
import static wci.frontend.CMinus.CMinusErrorCode.MISSING_LEFT_BRACE;
import static wci.frontend.CMinus.CMinusErrorCode.UNEXPECTED_TOKEN;
import static wci.frontend.CMinus.CMinusTokenType.LEFT_BRACE;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.ROUTINE_ICODE;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.ROUTINE_SYMTAB;
import static wci.message.MessageType.PARSER_SUMMARY;

/**
 * <h1>CMinusParserTD</h1>
 * <p/>
 * <p>The top-down CMinus parser.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class CMinusParserTD extends Parser {
    protected static CMinusErrorHandler errorHandler = new CMinusErrorHandler();

    private SymTabEntry routineId;  // name of the routine being parsed

    /**
     * Constructor.
     *
     * @param scanner the scanner to be used with this parser.
     */
    public CMinusParserTD(Scanner scanner) {
        super(scanner);
    }

    /**
     * Constructor for subclasses.
     *
     * @param parent the parent parser.
     */
    public CMinusParserTD(CMinusParserTD parent) {
        super(parent.getScanner());
    }

    /**
     * Getter.
     *
     * @return the routine identifier's symbol table entry.
     */
    public SymTabEntry getRoutineId() {
        return routineId;
    }

    /**
     * Getter.
     *
     * @return the error handler.
     */
    public CMinusErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /**
     * Parse a CMinus source program and generate the symbol table
     * and the intermediate code.
     *
     * @throws Exception if an error occurred.
     */
    public void parse()
            throws Exception {
        long startTime = System.currentTimeMillis();


        CMinusPredefined.initialize(symTabStack);

        try {
            Token token = nextToken();

            wci.frontend.CMinus.parsers.ProgramParser programParser = new wci.frontend.CMinus.parsers.ProgramParser(this);
            programParser.parse(token);

            token = currentToken();

            // Send the parser summary message.
            float elapsedTime = (System.currentTimeMillis() - startTime) / 1000f;
            sendMessage(new Message(PARSER_SUMMARY,
                    new Number[]{token.getLineNumber(),
                            getErrorCount(),
                            elapsedTime}));
        } catch (java.io.IOException ex) {
            errorHandler.abortTranslation(IO_ERROR, this);
        }
    }

    /**
     * Return the number of syntax errors found by the parser.
     *
     * @return the error count.
     */
    public int getErrorCount() {
        return errorHandler.getErrorCount();
    }

    /**
     * Synchronize the parser.
     *
     * @param syncSet the set of token types for synchronizing the parser.
     * @return the token where the parser has synchronized.
     * @throws Exception if an error occurred.
     */
    public Token synchronize(EnumSet syncSet)
            throws Exception {
        Token token = currentToken();

        // If the current token is not in the synchronization set,
        // then it is unexpected and the parser must recover.
        if (!syncSet.contains(token.getType())) {

            // Flag the unexpected token.
            errorHandler.flag(token, UNEXPECTED_TOKEN, this);

            // Recover by skipping tokens that are not
            // in the synchronization set.
            do {
                token = nextToken();
            } while (!(token instanceof EofToken) &&
                    !syncSet.contains(token.getType()));
        }

        return token;
    }
}
