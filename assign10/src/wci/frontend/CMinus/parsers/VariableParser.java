package wci.frontend.CMinus.parsers;

import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.symtabimpl.*;
import wci.intermediate.icodeimpl.*;
import wci.intermediate.typeimpl.*;
import wci.intermediate.symtabimpl.DefinitionImpl;

import static wci.frontend.CMinus.CMinusTokenType.*;
import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.intermediate.symtabimpl.DefinitionImpl.*;
import static wci.intermediate.typeimpl.TypeFormImpl.ARRAY;
import static wci.intermediate.typeimpl.TypeFormImpl.RECORD;
import static wci.intermediate.typeimpl.TypeFormImpl.SCALAR;
import static wci.intermediate.typeimpl.TypeKeyImpl.*;
import static wci.intermediate.icodeimpl.ICodeNodeTypeImpl.*;
import static wci.intermediate.icodeimpl.ICodeKeyImpl.*;

/**
 * <h1>VariableParser</h1>
 * <p/>
 * <p>Parse a CMinus variable.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class VariableParser extends StatementParser {
    // Set to true to parse a function name
    // as the target of an assignment.
    private boolean isFunctionTarget = false;

    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    public VariableParser(CMinusParserTD parent) {
        super(parent);
    }

    // Synchronization set to start a subscript or a field.
    private static final EnumSet<CMinusTokenType> SUBSCRIPT_FIELD_START_SET =
            EnumSet.of(LEFT_BRACKET);

    /**
     * Parse a variable.
     *
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token)
            throws Exception {
        // Look up the identifier in the symbol table stack.
        String name = token.getText().toLowerCase();
        SymTabEntry variableId = symTabStack.lookup(name);

        // If not found, flag the error and enter the identifier
        // as an undefined identifier with an undefined type.
        if (variableId == null) {
            errorHandler.flag(token, IDENTIFIER_UNDEFINED, this);
            variableId = symTabStack.enterLocal(name);
            variableId.setDefinition(UNDEFINED);
            variableId.setTypeSpec(CMinusPredefined.undefinedType);
        }

        return parse(token, variableId);
    }

    /**
     * Parse a function name as the target of an assignment statement.
     *
     * @param token the initial token.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parseFunctionNameTarget(Token token)
            throws Exception {
        isFunctionTarget = true;
        return parse(token);
    }

    /**
     * Parse a variable.
     *
     * @param token      the initial token.
     * @param variableId the symbol table entry of the variable identifier.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    public ICodeNode parse(Token token, SymTabEntry variableId)
            throws Exception {
        // Check how the variable is defined.
        Definition defnCode = variableId.getDefinition();
        if (!((defnCode == DefinitionImpl.VARIABLE) || (defnCode == VALUE_PARM) ||
                (defnCode == VAR_PARM) ||
                (isFunctionTarget && (defnCode == DefinitionImpl.FUNCTION))
        )
                ) {
            errorHandler.flag(token, INVALID_IDENTIFIER_USAGE, this);
        }

        variableId.appendLineNumber(token.getLineNumber());

        ICodeNode variableNode =
                ICodeFactory.createICodeNode(ICodeNodeTypeImpl.VARIABLE);
        variableNode.setAttribute(ID, variableId);

        token = nextToken();  // consume the identifier

        // Parse array subscripts or record fields.
        TypeSpec variableType = variableId.getTypeSpec();
        while (SUBSCRIPT_FIELD_START_SET.contains(token.getType())) {
            ICodeNode subFldNode = token.getType() == LEFT_BRACKET
                    ? parseSubscript(variableType)
                    : parseField(variableType);
            token = currentToken();

            // Update the variable's type.
            // The variable node adopts the SUBSCRIPTS or FIELD node.
            variableType = subFldNode.getTypeSpec();
            variableNode.addChild(subFldNode);
        }

        variableNode.setTypeSpec(variableType);
        return variableNode;
    }

    // Synchronization set for the ] token.
    private static final EnumSet<CMinusTokenType> RIGHT_BRACKET_SET =
            EnumSet.of(RIGHT_BRACKET, EQUALS, SEMICOLON);

    /**
     * Parse a set of comma-separated subscript expressions.
     *
     * @param variableType the type of the array variable.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    private ICodeNode parseSubscript(TypeSpec variableType)
            throws Exception {
        Token token;
        ExpressionParser expressionParser = new ExpressionParser(this);

        token = nextToken();  // consume the [ or , token

        // Create a SUBSCRIPTS node.
        ICodeNode subscriptsNode = ICodeFactory.createICodeNode(SUBSCRIPTS);

        // Parse the subscript expression.
        ICodeNode exprNode = expressionParser.parse(token);
        TypeSpec exprType = exprNode != null ? exprNode.getTypeSpec()
                : CMinusPredefined.undefinedType;

        if (exprType == CMinusPredefined.integerType) {
            subscriptsNode.addChild(exprNode);

            // Update the variable's type.
            variableType =
                    (TypeSpec) variableType.getAttribute(ARRAY_ELEMENT_TYPE);
        } else {
            errorHandler.flag(token, INVALID_TYPE, this);
        }

        // Synchronize at the ] token.
        token = synchronize(RIGHT_BRACKET_SET);
        if (token.getType() == RIGHT_BRACKET) {
            token = nextToken();  // consume the ] token
        } else {
            errorHandler.flag(token, MISSING_RIGHT_BRACKET, this);
        }

        subscriptsNode.setTypeSpec(variableType);
        return subscriptsNode;
    }

    /**
     * Parse a record field.
     *
     * @param variableType the type of the record variable.
     * @return the root node of the generated parse tree.
     * @throws Exception if an error occurred.
     */
    private ICodeNode parseField(TypeSpec variableType)
            throws Exception {
        // Create a FIELD node.
        ICodeNode fieldNode = ICodeFactory.createICodeNode(ICodeNodeTypeImpl.FIELD);

        Token token = nextToken();  // consume the . token
        TokenType tokenType = token.getType();
        TypeForm variableForm = variableType.getForm();

        if ((tokenType == IDENTIFIER) && (variableForm == RECORD)) {
            SymTab symTab = (SymTab) variableType.getAttribute(RECORD_SYMTAB);
            String fieldName = token.getText().toLowerCase();
            SymTabEntry fieldId = symTab.lookup(fieldName);

            if (fieldId != null) {
                variableType = fieldId.getTypeSpec();
                fieldId.appendLineNumber(token.getLineNumber());

                // Set the field identifier's name.
                fieldNode.setAttribute(ID, fieldId);
            } else {
                errorHandler.flag(token, INVALID_FIELD, this);
            }
        } else {
            errorHandler.flag(token, INVALID_FIELD, this);
        }

        token = nextToken();  // consume the field identifier

        fieldNode.setTypeSpec(variableType);
        return fieldNode;
    }
}
