package wci.frontend.CMinus.parsers;

import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;

import static wci.frontend.CMinus.CMinusTokenType.*;

/**
 * <h1>TypeSpecificationParser</h1>
 *
 * <p>Parse a CMinus type specification.</p>
 *
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
class TypeSpecificationParser extends CMinusParserTD
{
    /**
     * Constructor.
     * @param parent the parent parser.
     */
    protected TypeSpecificationParser(CMinusParserTD parent)
    {
        super(parent);
    }

    // Synchronization set for starting a type specification.
    static final EnumSet<CMinusTokenType> TYPE_START_SET =
        DeclarationsParser.DECLARATION_START_SET.clone();
    static {
        TYPE_START_SET.add(LEFT_BRACKET);
    }

    /**
     * Parse a CMinus type specification.
     * @param token the current token.
     * @return the type specification.
     * @throws Exception if an error occurred.
     */
    public TypeSpec parse(Token token)
        throws Exception
    {
        // Synchronize at the start of a type specification.
        token = synchronize(TYPE_START_SET);

        switch ((CMinusTokenType) token.getType()) {

            case LEFT_BRACKET: {
                ArrayTypeParser arrayTypeParser = new ArrayTypeParser(this);
                return arrayTypeParser.parse(token);
            }

            default: {
                SimpleTypeParser simpleTypeParser = new SimpleTypeParser(this);
                return simpleTypeParser.parse(token);
            }
        }
    }
}
