package wci.frontend.CMinus.parsers;

import java.util.ArrayList;
import java.util.EnumSet;

import wci.frontend.*;
import wci.frontend.CMinus.CMinusTokenType.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.symtabimpl.DefinitionImpl;

import static wci.intermediate.symtabimpl.SymTabKeyImpl.ROUTINE_ICODE;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.ROUTINE_ROUTINES;
import static wci.intermediate.symtabimpl.SymTabKeyImpl.ROUTINE_SYMTAB;

/**
 * <h1>ProgramParser</h1>
 *
 * <p>Parse a CMinus program.</p>
 *
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public class ProgramParser extends DeclarationsParser
{
    private SymTabEntry routineId;  // name of the routine being parsed

    /**
     * Constructor.
     * @param parent the parent parser.
     */
    public ProgramParser(CMinusParserTD parent)
    {
        super(parent);
    }

    // Synchronization set to start a program.
    static final EnumSet<CMinusTokenType> PROGRAM_START_SET =
        DeclarationsParser.DECLARATION_START_SET.clone();

    /**
     * Parse a program.
     * @param token the initial token.
     * @return null
     * @throws Exception if an error occurred.
     */
    public SymTabEntry parse(Token token)
        throws Exception
    {
        ICode iCode = ICodeFactory.createICode();

        // Create a dummy program identifier symbol table entry.
        routineId = symTabStack.enterLocal("DummyProgramName".toLowerCase());
        routineId.setDefinition(DefinitionImpl.PROGRAM);
        symTabStack.setProgramId(routineId);

        // Push a new symbol table onto the symbol table stack and set
        // the routine's symbol table and intermediate code.
        routineId.setAttribute(ROUTINE_SYMTAB, symTabStack.push());
        routineId.setAttribute(ROUTINE_ICODE, iCode);
        routineId.setAttribute(ROUTINE_ROUTINES, new ArrayList<SymTabEntry>());

        token = synchronize(PROGRAM_START_SET);

        DeclarationsParser declarationsParser = new DeclarationsParser(this);
        declarationsParser.parse(token,routineId,false);

        token = currentToken();
        synchronize(EnumSet.of(CMinusTokenType.LEFT_BRACE));

        BlockParser blockParser = new BlockParser(this);
        iCode.setRoot(blockParser.parse(token,routineId));

        return null;
    }
}
