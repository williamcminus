package wci.frontend.CMinus.parsers;

import wci.frontend.*;
import wci.frontend.CMinus.*;
import wci.intermediate.*;
import wci.intermediate.symtabimpl.*;

import java.util.EnumSet;

import static wci.frontend.CMinus.CMinusErrorCode.*;
import static wci.intermediate.symtabimpl.DefinitionImpl.*;

/**
 * <h1>SimpleTypeParser</h1>
 * <p/>
 * <p>Parse a simple CMinus type (identifier, subrange, enumeration)
 * specification.</p>
 * <p/>
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
class SimpleTypeParser extends TypeSpecificationParser {
    /**
     * Constructor.
     *
     * @param parent the parent parser.
     */
    protected SimpleTypeParser(CMinusParserTD parent) {
        super(parent);
    }

    // Synchronization set for starting a simple type specification.
    static final EnumSet<CMinusTokenType> SIMPLE_TYPE_START_SET =
            DeclarationsParser.DECLARATION_START_SET.clone();

    /**
     * Parse a simple CMinus type specification.
     *
     * @param token the current token.
     * @return the simple type specification.
     * @throws Exception if an error occurred.
     */
    public TypeSpec parse(Token token)
            throws Exception {
        // Synchronize at the start of a simple type specification.
        token = synchronize(SIMPLE_TYPE_START_SET);

        if (SIMPLE_TYPE_START_SET.contains(token.getType())) {
            String name = token.getText().toLowerCase();
            SymTabEntry id = symTabStack.lookup(name);

            if (id != null) {
                Definition definition = id.getDefinition();

                // It's either a type identifier
                // or the start of a subrange type.
                if (definition == DefinitionImpl.TYPE) {
                    id.appendLineNumber(token.getLineNumber());

                    // Return the type of the referent type.
                    return id.getTypeSpec();
                } else if ((definition != CONSTANT) &&
                        (definition != ENUMERATION_CONSTANT)) {
                    errorHandler.flag(token, NOT_TYPE_IDENTIFIER, this);
                    token = nextToken();  // consume the identifier
                    return null;
                }
            } else {
                errorHandler.flag(token, IDENTIFIER_UNDEFINED, this);
                token = nextToken();  // consume the identifier
                return null;
            }
        } else {
            errorHandler.flag(token, UNEXPECTED_TOKEN, this);
            token = nextToken();
            return null;
        }
        return null;
    }
}
