PROGRAM Block(infile, outfile);

TYPE

    arr1 = ARRAY [0..10] OF integer;

VAR
    var1 : arr1;
    doggy : integer;

FUNCTION sum(VAR data : arr1; m : integer) : integer;

BEGIN
    sum := data[5] + m;
END;



BEGIN
    var1[5] := 1;
    doggy := 4;

    doggy := sum(var1,doggy);

    writeln('sum = ',doggy);
END.
