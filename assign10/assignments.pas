PROGRAM Assignments;

VAR
    five		   : integer;
    ratio, dze		   : real;
    fahrenheit, centigrade : real;
    number		   : integer;
    root		   : real;
    ch			   : char;
    str			   : ARRAY[1..12] OF char;
   idata		   : ARRAY[0..9] of integer;

BEGIN
    BEGIN {Temperature conversions.}
        five  := -1 + 2 - 3 + 4 + 3;
        ratio := five/9.0;

        fahrenheit := 72;
        centigrade := (fahrenheit - 32)*ratio;

        centigrade := 25;
        fahrenheit := centigrade/ratio + 32;

        centigrade := 25;
        fahrenheit := 32 + centigrade/ratio
    END;

    dze := fahrenheit/ratio;

    BEGIN {Calculate a square root using Newton's method.}
        number := 2;
        root := number;
        root := (number/root + root)/2;
        root := (number/root + root)/2;
        root := (number/root + root)/2;
        root := (number/root + root)/2;
        root := (number/root + root)/2;
    END;

   ch  := 'x';
   str := 'hello, world';
   writeln(str);
   idata[0] := 1;
   writeln(idata[0]);
   idata[1] := idata[0];
   writeln(idata[1])
END.
